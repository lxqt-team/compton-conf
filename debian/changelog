compton-conf (0.16.0-4) unstable; urgency=medium

  [ Macy Lin ]
  * debian/control: build-deps on qttools5-dev. (Closes: #1078275)

  [ Andrew Lee (李健秋) ]
  * debian/salsa-ci.yml: add file.
  * Clean up of group membership after 2nd calls.
  * debian/copyright: update.
  * debian/control: bump to Standards-version to 4.7.0, no changes needed.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Sat, 07 Sep 2024 10:32:03 +0200

compton-conf (0.16.0-3) unstable; urgency=medium

  * Bump Standards-Version to 4.6.2.
  * Bump compat to 13.
  * Set Rules-Requires-Root to no.
  * Change team email to <team+lxqt@tracker.debian.org>.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Thu, 15 Jun 2023 01:56:50 +0800

compton-conf (0.16.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + compton-conf-l10n: Drop versioned constraint on compton-conf in Replaces.
    + compton-conf-l10n: Drop versioned constraint on compton-conf in Breaks.

  [ Andrew Lee (李健秋) ]
  * debian/watch: fix uscan error.
  * debian/rules: Remove unnecessary -Wl,--as-needed.
  * debian/rules: Drop --fail-missing argument to dh_missing which is now
    default in debhelper 13.
  * debian/upstream/metadata: drop useless Repository-Browser line.
  * debian/lintian-overrides: fix overrides for no-manual-page.
  * debian/lintian-overrides: fix overrides for desktop-entry-invalid-category.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Fri, 04 Nov 2022 13:25:24 +0800

compton-conf (0.16.0-1) unstable; urgency=medium

  [ Alf Gaida ]
  * Switched to gbp
  * Bumped Standards-Version to 4.4.0, no changes needed

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.

  [ Andrew Lee (李健秋) ]
  * New upstream release. (Closes: #978202)

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Thu, 07 Jan 2021 19:48:45 +0800

compton-conf (0.14.1-1) unstable; urgency=medium

  * Cherry-picked upstream release 0.14.1.

 -- Alf Gaida <agaida@siduction.org>  Tue, 26 Feb 2019 00:17:09 +0100

compton-conf (0.14.0-1) unstable; urgency=medium

  * Cherry-picked upstream release 0.14.0.
  * Bumped Standards to 4.3.0, no changes needed
  * Dropped d/compat, use debhelper-compat = 12, no changes needed
  * Fixed years in d/copyright
  * Bumped minimum version lxqt-build-tools (>= 0.6.0~)
  * Removed obsolete PULL_TRANSLATIONS= OFF from dh_auto_configure
  * Added l10n-package, moved from lxqt-l10n
  * Added d/upstream/metadata

 -- Alf Gaida <agaida@siduction.org>  Sun, 27 Jan 2019 14:29:38 +0100

compton-conf (0.4.0-1) unstable; urgency=medium

  * Cherry-picked upstream release 0.4.0
  * Bump lxqt-build-tools to >= 0.5.0~
  * Fixed watch file lxde -> lxqt

 -- Alf Gaida <agaida@siduction.org>  Wed, 23 May 2018 18:53:53 +0200

compton-conf (0.3.0-6) unstable; urgency=medium

  * Changed VCS fields for salsa
  * Bumped Standards to 4.1.4, no changes needed
  * Bumped compat to 11
  * Bumped debhelper to >= 11~
  * Fixed homepage and copyright years

 -- Alf Gaida <agaida@siduction.org>  Wed, 25 Apr 2018 01:43:49 +0200

compton-conf (0.3.0-5) unstable; urgency=medium

  * Bumped Standards to 4.1.2, no changes needed
  * Removed debian/gbp.conf - we use git stuff
  * Fixed trailing whitespaces in changelog
  * fixed lintian whining about binary-control-field-duplicates-source

 -- Alf Gaida <agaida@siduction.org>  Wed, 13 Dec 2017 19:21:02 +0100

compton-conf (0.3.0-4) unstable; urgency=medium

  * Removed branch from VCS-Fields, makes no sense at all
  * Fixed typo in changelog

 -- Alf Gaida <agaida@siduction.org>  Wed, 06 Dec 2017 19:33:37 +0100

compton-conf (0.3.0-3) unstable; urgency=medium

  * Transition to unstable

 -- Alf Gaida <agaida@siduction.org>  Mon, 04 Dec 2017 18:16:21 +0100

compton-conf (0.3.0-2) experimental; urgency=medium

  * Bumped standards to 4.1.1 - no changes needed
  * Changed priority to optional

 -- Alf Gaida <agaida@siduction.org>  Fri, 20 Oct 2017 17:02:07 +0200

compton-conf (0.3.0-1) experimental; urgency=medium

  * Cherry-picked upstream release 0.3.0
  * Switched to experimental
  * Bumped build-dependency lxqt-build-tools (>= 0.4.0)
  * Bumped Standards to 4.1.0, no changes needed
  * Bumped years in copyright
  * Added Breaks/Replaces for lxqt-common

 -- Alf Gaida <agaida@siduction.org>  Sun, 24 Sep 2017 02:06:51 +0200

compton-conf (0.2.1-2) unstable; urgency=medium

  * Changed Priority to extra - compton is extra

 -- Alf Gaida <agaida@siduction.org>  Mon, 09 Jan 2017 20:44:56 +0100

compton-conf (0.2.1-1) unstable; urgency=medium

  * Cherry-picking new upstream-release 0.2.1.
  * Removed build deprndency liblxqt0-dev
  * Bumped build-dependency lxqt-build-tools (>= 0.3.0)
  * Added dependency compton

 -- Alf Gaida <agaida@siduction.org>  Sun, 11 Dec 2016 19:29:46 +0100

compton-conf (0.2.0-3) unstable; urgency=medium

  * Fixed VCS branches to sid
  * Added build dependency lxqt-build-tools
  * Removed some build dependencies:
    - cmake
    - libqt5xdg-dev
    - libqt5xdgiconloader-dev
    - pkg-config
    - qttools5-dev
    - qttools5-dev-tools

 -- Alf Gaida <agaida@siduction.org>  Fri, 04 Nov 2016 22:04:50 +0100

compton-conf (0.2.0-2) unstable; urgency=medium

  * Cherry-picking new upstream release 0.2.0.
  * Sync debian/foo from experimental
  * Set compat to 10
  * Minimim version debhelper (>= 10)
  * Removed --parallel from rules, standard compat 10
  * Added some build dependencies:
    - libkf5windowsystem-dev,
    - liblxqt0-dev (>= 0.11.0),
    - libqt5svg5-dev
    - libqt5x11extras5-dev,
    - libqt5xdg-dev (>= 2.0.0),
    - libqt5xdgiconloader-dev (>= 2.0.0),
  * Added Recommends compton-conf-l10n
  * Added translation control to rules
  * Set CMAKE_BUILD_TYPE=RelWithDebInfo

 -- Alf Gaida <agaida@siduction.org>  Mon, 17 Oct 2016 22:37:48 +0200

compton-conf (0.1.0+20151226-3) unstable; urgency=medium

  * Bumped Standards to 3.9.8, no changes needed
  * Fixed VCS fields, use plain /git/, branch added
  * Fixed copyrights Format field, use https
  * Exported LC_ALL=C.UTF-8, make builds reproducible

 -- Alf Gaida <agaida@siduction.org>  Tue, 19 Jul 2016 00:20:00 +0200

compton-conf (0.1.0+20151226-2) unstable; urgency=medium

  * Bump Standards to 3.9.7
  * Add hardening=+all
  * Secure VCS-fields
  * Fix year in copyright
  * Fix new grep behaviour, thanks to Eduard Sanou for the bug and
    the patch, but i already fixed this upstream (Closes: #815901)

 -- Alf Gaida <agaida@siduction.org>  Fri, 26 Feb 2016 01:49:45 +0100

compton-conf (0.1.0+20151226-1) unstable; urgency=medium

  * Cherry-picking upstream version 0.1.0+20151226.
  * Removed GPL from copyright, compton-conf is LGPL only
  * Cleaned up rules

 -- Alf Gaida <agaida@siduction.org>  Sat, 26 Dec 2015 13:23:56 +0100

compton-conf (0.1.0+20150831-1) unstable; urgency=medium

  [ ChangZhuo Chen (陳昌倬) ]
  * Remove compton-conf-qt5 related Replace, Break. (Closes: #798337)
  * Remove unnecessary dependencies.

  [ Alf Gaida ]
  * run cme fix dpkg-control
  * fixed bug in desktop file upstream, resolve lintian warning
    duplicated-key-in-desktop-entry

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Fri, 11 Sep 2015 21:04:13 +0800

compton-conf (0.1.0+20150626-1) unstable; urgency=medium

  [ Alf Gaida ]
  * initial debian files.
  * Initial release (Closes: #756359)

  [ Wen.Liao ]
  * Remove lintian warning in debian/copyright.

  [ Alf Gaida ]
  * - bump standards to 3.9.6 - Min Qt version 5.3.2 - no source changes -
    drop debug package - lintian-override for missing manpage and invalid
    category added.
  * fixed debian $foo packages renamed source/options added.
  * Some cleanup in debian $foo.
  * some debian $foo improvements.

  [ Andrew Lee (李健秋) ]
  * Corrected GPL2 copyright instead of GPL2+.
  * Added myself as Uploader.
  * Removing whitespaces at EOL and EOF.
  * Merging upstream version 0.1.0+20150626.

 -- Andrew Lee (李健秋) <ajqlee@debian.org>  Fri, 14 Aug 2015 15:03:55 +0800
